sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (baseController, JSONModel, MessageBox) {
	"use strict";

	return baseController.extend("rimmy.kakao.map.controller.Detail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf rimmy.kakao.map.view.Detail
		 */
		onInit: function () {
			// debugger
			var router = this.getRouter();
			router.getRoute("Detail").attachMatched(this._onRouteMatched, this);
		},
		
		_onRouteMatched : function (oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			// debugger;
			// 보낼때 데이터를 보내자나,,? json모양인 데이터를 보낸단 말야,,,
			//그럼 그걸 받자마자 model에 set하면 될 것 같은데,,,
			// oArgs에 아무것도 들어오지 않아,,,,

		// 	oView.bindElement({
		// 		        /*아이템 경로,,*/
		// 		path : "/Employees(" + oArgs.employeeId + ")",
		// 		events : {
		// 			change: this._onBindingChange.bind(this),
		// 			dataRequested: function (oEvent) {
		// 				oView.setBusy(true);
		// 			},
		// 			dataReceived: function (oEvent) {
		// 				oView.setBusy(false);
		// 			}
		// 		}
		// 	});
		},
		// _onBindingChange : function (oEvent) {
		// 	// No data for the binding
		// 	if (!this.getView().getBindingContext()) {
		// 		this.getRouter().getTargets().display("notFound");
		// 	}
		// }

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf rimmy.kakao.map.view.Detail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf rimmy.kakao.map.view.Detail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf rimmy.kakao.map.view.Detail
		 */
		//	onExit: function() {
		//
		//	}

	});

});