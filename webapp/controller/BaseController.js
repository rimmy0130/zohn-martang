sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel"
], function(Controller, History, UIComponent, MessageBox, JSONModel) {
	"use strict";

	return Controller.extend("rimmy.kakao.map.controller.baseController", {
		
		oninit : function(){
		},

		getRouter : function () {
			return UIComponent.getRouterFor(this);
		},

		iconBackPress: function () { debugger
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				// this.resetCreateData();
				window.history.go(-1);
			} else {
				// this.resetCreateData();
				this.getRouter().navTo("Main", {}, true /*no history*/);
			}
		},
		
		//오늘 날짜 계산
		getTodayDateTime : function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var day = date.getDate();
			var hour = date.getHours();
			var minute = date.getMinutes();
			var second = date.getSeconds();
			
			// debugger
			// 한자리수일 경우 0을 채워준다. 
			if (month < 10) {
				month = "0" + month;
			}
			if (day < 10) {
				day = "0" + day;
			}
			if (hour < 10) {
				hour = "0" + hour;
			}
			if (minute < 10) {
				minute = "0" + minute;
			}
			if (second < 10) {
				second = "0" + second;
			}
			
			return year+month+day+hour+minute+second ;
		},
		
	
		
		//생성하지 않고 취소/뒤로가기 눌렀을때 생성하려던 데이터 reset
		resetCreateData : function(){
			this.getView().byId("placeName").setValue("");
			this.getView().byId("newAddr").setValue("");
			this.getView().byId("region").setSelectedKey("");
			this.getView().byId("placeNickName").setValue("");
			this.getView().byId("priceSlider").setValue(0);
			this.getView().byId("priceValue").setValue("");
			this.getView().byId("foodkind").setSelectedKey("");
			this.getView().byId("recoMan").setValue("");
			this.getView().byId("rating").setValue(3);
			this.getView().byId("comment").setValue("");
			// this.getView().byId("ateDate").setValue("");
			
		}

	});

});