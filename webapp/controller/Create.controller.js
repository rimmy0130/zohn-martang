sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
], function (baseController, JSONModel, MessageBox, Fragment ) {
	"use strict";
	var xyArr = [];
	return baseController.extend("rimmy.kakao.map.controller.Create", {
		onInit: function () {debugger;
			this.router = this.getRouter();
			this.getView().byId("region").getIcon().setProperty("src","sap-icon://search");
			
			this.router.getRoute("Create").attachPatternMatched(this._onCreateMatched, this);
		},
		_onCreateMatched : function() {
			this.resetCreateData();
			this.createPageMakeMap(127.11674377596071 , 37.484177185974836, false, null);
			this.byId("ateDate").setValue(this.getTodayDateTime().substring(0,8));
		},
		
		onBeforeRendering : function(){
			
		},
		onAfterRendering : function(){  debugger;
			// this.createPageMakeMap(127.11674377596071 , 37.484177185974836, false, null);
		},
		
		createPageMakeMap : function(x,y,draggable,pName){
			var divId = this.getView().byId("mapContent").getId();
			var container = document.getElementById(divId);
			var Latitude =  x //위도 
			var longitude =  y //경도
			
			this.getView().byId("mapInfo").setText(pName);
			var infoDivId = this.getView().byId("mapInfo").getId();
			var infoContainer = document.getElementById(infoDivId);
			
			var position  = new kakao.maps.LatLng(longitude, Latitude); 
			var options = {
				center: position,
				level: 3 
			};
			
			if(!this.createMap){
				this.createMap = new kakao.maps.Map(container, options);
				//지도 이동 유무
				this.createMap.setDraggable(draggable);
				// 마커를 생성합니다
				this.createMarker = new kakao.maps.Marker({
					map : this.createMap,
				    position: position,
				    clickable: true 
				});
				//마커를 위치시킴
				this.createMarker.setMap(this.createMap);
				
				this.createInfowindow = new kakao.maps.InfoWindow({
				    position : position, 
				    content : infoContainer
				});
				
				if(pName){
					this.createInfowindow.open(this.createMap, this.createMarker); 
				}
			}else{
				//지도 이동 유무
				this.createMap.setDraggable(draggable);
			    // 지도 중심을 이동 시킵니다
			    this.createMap.setCenter(position);
			    // 지도 중심을 부드럽게 이동시킵니다
			    this.createMap.panTo(position);    
			    // 마커 이동
			    this.createMarker.setPosition(position);
			    this.createInfowindow.setPosition(position);
			}
		},
				
		onPriceChange : function(){
			var selectedPrice = this.getView().byId("priceSlider").getValue();
			this.getView().byId("priceValue").setValue(selectedPrice);
		},
		
		onRequiredChange : function(event){
			//필수값 체크 
			var placeName  = this.getView().byId("placeName").getValue().replace(/ /gi, "");
			var newAddr    = this.getView().byId("newAddr").getValue().replace(/ /gi, "");
			var priceValue = this.getView().byId("priceValue").getValue().replace(/ /gi, "");
			var isPriceValue = priceValue > 0 ? true : false ;
			var rating     = this.getView().byId("rating").getValue();
			
			
			//조건들을 다 통과하면 저장버튼 활성화
			if( placeName && newAddr && priceValue && isPriceValue && rating ){
				this.getView().byId("idSave").setEnabled(true);
			}else{
				this.getView().byId("idSave").setEnabled(false);
			}
		},
		
		onChange: function (oEvent) {
			var validChk = oEvent.getParameter("valid") === false || !oEvent.getParameter("newValue") ?  false : true ;
			var returnResult = this.dateValidationCheck(validChk);
			if(returnResult){
				this.onRequiredChange();                   
			}

		},
		
		
		dateValidationCheck: function(validChk) {
			if (!validChk) { 
				this.byId("ateDate").setValueState("Error"); 
				this.byId("ateDate").setValueStateText("정상적인 날짜(yyyymmdd)를 입력해주세요.");
				this.byId("ateDate").setValue("");
				this.byId("ateDate").setPlaceholder("yyyymmdd");
				return false;
			}else{
				this.byId("ateDate").setValueState("None"); 
				this.newDate = this.byId("ateDate").getValue();   //oEvent.getParameter("newValue");
				return true;
			}
		},
		
		onSave : function(event){
			var that = this;
			// var foodkind = this.getView().byId("foodkind")
			// if( foodkind.getSelectedKey()=== "All"){
			// 	foodkind.setSelectedKey("");
			// }
			
			debugger
			
			$.ajax({
				url : "/map_rimmy/api/v1/create",
				method : "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				},
				data: JSON.stringify({
					"placeName"	:	this.getView().byId("placeName").getValue(),
					"address"	:	this.getView().byId("newAddr").getValue(),
					"region"	:	this.getView().byId("region").getSelectedKey(),
					"placeNickName"	:	this.getView().byId("placeNickName").getValue(),
					"price"		:	this.getView().byId("priceValue").getValue(),
					"foodKind"	:	this.getView().byId("foodkind").getSelectedKey(),
					"recoMan"	:	this.getView().byId("recoMan").getValue(),
					"rating"	:	this.getView().byId("rating").getValue().toString(),
					"comment"	:	this.getView().byId("comment").getValue(),
					"date"		:   this.getTodayDateTime(),
					"xyData"	:	xyArr
					
				}),
				beforeSend: function () { 
					that.getView().byId("createPage").setBusy(true);
				},
				success : function(data,headers){ 
					if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
					//정상이면 저장되었습니다 메세지 띄울 것 
					if(data.status === "Success"){
						sap.m.MessageBox.success(data.msg, {
						    title: "확인",
						    onClose: function(OK){that.router.navTo("Main"); }
						});
						that.getView().byId("createPage").setBusy(false);
						that.resetCreateData();
					}
				},
				error : function(data){
					// MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
					MessageBox.error(data.msg);
				}
			});
			
			
		},
		
		
		
		onReject : function(event){
			//새로운 맛집 취소하고 나감
			// 페이지의 값 다 지우기
			this.resetCreateData();
			
			//메인으로 가기
			// this.router.navTo("Main");
			this.iconBackPress();
		},
		
		//팝업 확인	
		onOK: function(oEvent) { //선택된 값 물고 내려오기
			this.getView().byId("placeNickName").setValue(this.getView().byId("placeName").getValue());
            this.getView().byId("newAddr").setValue(this.mapData.road_addr);
            this.getView().byId("placeName").setValue(this.mapData.place_name);
            var yData = this.mapData.map.y ;
            var xData = this.mapData.map.x ;
            xyArr=[];
            xyArr.push(yData);
            xyArr.push(xData);
            //지도를 다시 로드
            this.createPageMakeMap(this.mapData.map.x, this.mapData.map.y, false, this.mapData.place_name);
            
			oEvent.getSource().close();
            oEvent.getSource().destroy();
		},

		//팝업 취소
		onCancel: function(oEvent) {
			oEvent.getSource().close();
            oEvent.getSource().destroy();
		},
		
		//팝업 로드
		// onLoad: function(){
		// 	this.makeMapPopup(126.923919460392 , 37.5563194254356);
		// 	// oEvent.getSource()
		// },
		
		makeMapPopup: function(x,y,pName){
			var divId = this.byId("mapPopup").getId();
			var container = document.getElementById(divId);
			var Latitude =  x //위도 
			var longitude =  y //경도
			
			var positionPop  = new kakao.maps.LatLng(longitude, Latitude); 
			var options = {
				center: positionPop,
				level: 3 
			};
			var mapPop = new kakao.maps.Map(container, options);
			// 마커를 생성합니다
			var markerPop = new kakao.maps.Marker({
				map : mapPop,
			    position: positionPop,
			    clickable: true 
			});
			//마커를 위치시킴
			markerPop.setMap(mapPop);
			
			// 마커를 클릭했을 때 마커 위에 표시할 인포윈도우를 생성합니다
			// 인포윈도우에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
			if(pName){ //pName이 있다면 
				this.byId("mapInfoPop").setText(pName);
				var infoPopId = this.byId("mapInfoPop").getId();
				var infoPopContainer = document.getElementById(infoPopId);
				// var infoTextId = this.getView().byId("infoText").getId();
				// document.getElementById(infoTextId).innerHTML= "찍은좌표의 가게이름";
				// infoContainer.innerHTML= pName;
				debugger
				
				var infowindow = new kakao.maps.InfoWindow({
				    map: mapPop, // 인포윈도우가 표시될 지도
				    position : positionPop, 
				    content : infoPopContainer
			    // ,removable : true
				});
				
				//마커위에 인포윈도우를 띄우려면
				infowindow.open(mapPop, markerPop); 
			}

			
			// var infowindowPop = new kakao.maps.InfoWindow({
		 //   	content : infoContainer
			// });
			
			
			// // 마커에 클릭이벤트를 등록합니다
			// window.kakao.maps.event.addListener(markerPop, "click", function() {
		 //   	infowindowPop.open(mapPop, markerPop);  
			// });
			
		},
		
		//주소 검색 버큰 클릭, 팝업 생성
		onSearchPop : function(){
			var placeName = this.getView().byId("placeName").getValue();
			if(!placeName || placeName.replace(/ /gi, "").length < 2){
				this.getView().byId("placeName").setValue("");
				MessageBox.error("검색어를 2글자 이상 입력해주세요.");
				return;
			}
			
			var oView = this.getView();
			// var oAddrpop = oEvent.getSource();
			if (!this.byId("addrDialog")) {
				Fragment.load({
					name: "rimmy.kakao.map.view.AddrPop",
					controller: this,
					id : oView.getId()
				}).then(function(oDialog){
					oView.addDependent(oDialog);
					this.byId("addrDialog").setTitle("주소 검색");
					this.byId("keywordInput").setValue(placeName);
					this.onPopSearchPress();
					
					this._oDialog = oDialog; //else의 open을 해주기 위해
					oDialog.open();
				}.bind(this));
			} else {
				this._oDialog.open();
			}
		},
		
		onPopSearchPress : function(){
			this.getMapPopList();
		},
		
		//팝업 내에서 검색했을때
		getMapPopList : function(){  debugger
			var that = this;
			var popItemList = this.byId("popItemList");
			var keywordInput = this.byId("keywordInput").getValue();
			
			if(!keywordInput || keywordInput.replace(/ /gi, "").length < 2){
				this.getView().byId("keywordInput").setValue("");
				MessageBox.error("검색어를 2글자 이상 입력해주세요.");
				return;
			}
			
			$.ajax({
				url : "/map_rimmy/api/v1/place",
				method : "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				},
				data: JSON.stringify({
					"placeName": keywordInput
				}),
				beforeSend: function () { 
					popItemList.setBusy(true);
				},
				success : function(data,headers){ 
					if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
					if(data.data.metadata.total_count === 0) {//데이터가 없으면
						that.byId("popItemList").setNoDataText("검색된 데이터가 없습니다.");
						that.makeMapPopup(127.11674377596071 , 37.484177185974836, null);
						
					}else{//데이터가 있으면
						//데이터 바인딩
						if(!that.getView().getModel("mappoplist")){
							var mapModel  = new JSONModel(data.data);
							that.getView().setModel(mapModel, "mappoplist");
						}else{
							that.getView().getModel("mappoplist").setData(data.data);
						}
						
						//데이터 바인딩 후 첫번째 리스트 값을 지도에 셋팅
						var mapPopXyData = that.getView().getModel("mappoplist").getData().mapdata[0];
						//리스트 클릭
						that.getView().byId("popItemList").getAggregation("items")[0].setHighlight("Information");
						//지도에 셋팅
						that.makeMapPopup(mapPopXyData.map.x , mapPopXyData.map.y, mapPopXyData.place_name);
						
					}

					//로딩바 끝
					popItemList.setBusy(false);
				},
				error : function(){
					popItemList.setBusy(false);
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		onPopItemPress : function(oEvent){
			var selectedItemIdx = oEvent.getSource().getBindingContextPath().split("/").reverse()[0];
			//클릭된 값 highlight 바꾸기
			var popListItem = oEvent.getSource().getParent().getAggregation("items");
			for(var i=0; i<popListItem.length;i++){
				if(popListItem[i].getHighlight()!=="None"){
					popListItem[i].setHighlight("None");
					break;
				}
			}
			oEvent.getSource().setHighlight("Information");
			
			//지도 바꾸기
			this.mapData = this.getView().getModel("mappoplist").getData().mapdata[selectedItemIdx];
			var xyData = this.mapData.map ;
			var pName = this.mapData.place_name;
			this.makeMapPopup(xyData.x, xyData.y, pName);
			
			
		}
		
		
	});
});