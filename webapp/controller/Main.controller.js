sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	// "./baseController",
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	'sap/ui/core/Fragment'
], function (baseController, JSONModel, MessageBox, Fragment) {
	"use strict";

	return baseController.extend("rimmy.kakao.map.controller.Main", {
		onInit: function () {
			this.router = this.getRouter();
			this.router.getRoute("Main").attachPatternMatched(this._onObjectMatched, this);
		},
		
		_onObjectMatched : function(oEvent){
			// debugger;
			this._getSavedDataList();			// DB Access 
			
		},
		
		onBeforeRendering : function(){
			// this.getSavedDataList();	
		},
		onAfterRendering : function(){  //debugger;
			// this._getCategory();
			// debugger;
			this._getSavedDataList();
			
			//makeMap하기 전에 조건에 맞는 데이터 검색 후 거기에 대한 pname값 입력할 것
			// this.makeMap(127.11674377596071 , 37.484177185974836, null ,true);
		},
		
		makeMap : function (x,y,pName,draggable) { 
			var divId = this.getView().byId("mapContent").getId();
			var container = document.getElementById(divId);
			var Latitude =  x //위도 
			var longitude =  y //경도
			
			// 마커를 클릭했을 때 마커 위에 표시할 인포윈도우를 생성합니다
			// 인포윈도우에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
			this.getView().byId("mapInfo").setText(pName);
			var infoDivId = this.getView().byId("mapInfo").getId();
			var infoContainer = document.getElementById(infoDivId);
			
			var position  = new kakao.maps.LatLng(longitude, Latitude); 
			var options = {
				center: position,
				level: 3 
			};
			
			if(!this.mainMap){
				this.mainMap = new kakao.maps.Map(container, options);
				//지도 이동 유무
				this.mainMap.setDraggable(draggable);
				// 마커를 생성합니다
				this.mainMarker = new kakao.maps.Marker({
					map : this.mainMap,
				    position: position
				});
				//마커를 위치시킴
				this.mainMarker.setMap(this.mainMap);
				//인포윈도우 생성
				this.infowindow = new kakao.maps.InfoWindow({
				    //map: this.mainMap, // 인포윈도우가 표시될 지도
				    position : position, 
				    content : infoContainer
				});
				//인포윈도우 위치시킴
				this.infowindow.open(this.mainMap, this.mainMarker);
			}else{
				//지도 이동 유무
				this.mainMap.setDraggable(draggable);
			    // 지도 중심을 이동 시킵니다
			    this.mainMap.setCenter(position);
			    // 지도 중심을 부드럽게 이동시킵니다
			    this.mainMap.panTo(position);    
			    // 마커 이동
			    this.mainMarker.setPosition(position);
			    //인포윈도우 이동
			    this.infowindow.setPosition(position);
			}
		},
		
		onAllMap : function(){
			
		},
		
		
		onListItemPress : function(oEvent){ 
			var selectedItemIdx = oEvent.getSource().getBindingContextPath().split("/").reverse()[0];
			// var detailData = new JSONModel = this.getView().getModel('savedMaplist').getData()[selectedItemIdx] ;
			// var oCtx = oEvent.getSource().getBindingContext();
			
			// var oItem, oCtx;
			// oItem = oEvent.getSource();
			// oCtx = oItem.getBindingContext();
			// this.getRouter().navTo("employee",{
			// 	employeeId : oCtx.getProperty("EmployeeID")
			// });
			// JSON.stringify(detailData)
			// debugger
			// oEvent.getParameter("view").setModel(this.getView().getModel('savedMaplist') ,'detailMapData' );
			// this.getRouter().navTo("Detail",{
			// 	detailData: selectedItemIdx
			// });
			
			
			
			//클릭된 값 highlight 바꾸기
			var mainListItem = oEvent.getSource().getParent().getAggregation("items");
			for(var i=0; i<mainListItem.length;i++){
				if(mainListItem[i].getHighlight()!=="None"){
					mainListItem[i].setHighlight("None");
					break;
				}
			}
			oEvent.getSource().setHighlight("Information");

			//지도 바꾸기
			var xyData = this.getView().getModel('savedMaplist').getData()[selectedItemIdx].xyData ;
			var pName = this.getView().getModel('savedMaplist').getData()[selectedItemIdx].placeName;
			this.makeMap(xyData[1], xyData[0], pName ,true);
			
		},
		
		// _getCategory : function(){ 
		// 	var that = this;
		// 	this.getView().byId("combocate").getPicker().setContentHeight("300px");
		// 	$.ajax({
		// 		url : "/map_rimmy/api/v1/category/code",
		// 		method : "GET",
		// 		success : function(data,headers){
		// 			var codeModel = new JSONModel(data);
		// 			that.getView().setModel(codeModel, "categories");
		// 			that.getView().byId("combocate").setSelectedKey("All");
		// 			if(!that.getView().byId("combocate").getEnabled()){
		// 				that.getView().byId("combocate").setEnabled(true);
		// 			}
					
		// 		},
		// 		error : function(err){
		// 			MessageBox.error("카테고리 불러오는데 실패했어요,,,,,,");
		// 			that.getView().byId("combocate").setEnabled(false);
		// 		}
		// 	});
		// },
		
		onSearchPress : function(oEvent){
			// this._getMapList();
		},
		
		onDateChange : function(oEvent){
			var dateFrom = oEvent.getParameter("from");
			var dateTo = oEvent.getParameter("to");
			var valid = oEvent.getParameter("valid");

			if (valid) {
				oEvent.getSource().setValueState(this.getView().byId("seleteDateRange").setValueState("None"));
			} else {
				oEvent.getSource().setValueState(this.getView().byId("seleteDateRange").setValueState("Error"));
			}
		},
		
		// _getMapList : function(){  debugger
		// 	var that = this;
		// 	var mapListItem = this.byId("mapitemlist");
		// 	var placeName = this.getView().byId("keywordInput").getValue();
		//     // placeName = placeName.replace(/ /gi, "");
		// 	var cateCode = this.getView().byId("combocate").getSelectedKey();
		// 	if(!placeName || placeName.replace(/ /gi, "").length < 2){
		// 		this.getView().byId("keywordInput").setValue("");
		// 		MessageBox.error("검색어를 2글자 이상 입력해주세요.");
		// 		return;
		// 	}
		// 	if(cateCode === "All"){cateCode = "";}
			
		// 	$.ajax({
		// 		url : "/map_rimmy/api/v1/place",
		// 		method : "POST",
		// 		headers: {
		// 			"Content-Type": "application/json; charset=utf-8"
		// 		},
		// 		data: JSON.stringify({
		// 			"placeName": placeName,
		// 			"cateCode" : cateCode
		// 		}),
		// 		beforeSend: function () { 
		// 			mapListItem.setBusy(true);
		// 		},
		// 		success : function(data,headers){ 
		// 			if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
		// 			if(data.data.metadata.total_count === 0) {that.byId("mapitemlist").setNoDataText("검색된 데이터가 없습니다.");}
		// 			//데이터 바인딩
		// 			if(!that.getView().getModel("maplist")){
		// 				var mapModel  = new JSONModel(data.data);
		// 				that.getView().setModel(mapModel, "maplist");
		// 			}else{
		// 				that.getView().getModel("maplist").setData(data.data);
		// 			}
		// 			mapListItem.setBusy(false);
		// 		},
		// 		error : function(){
		// 			mapListItem.setBusy(false);
		// 			MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
		// 		}
		// 	});
		// },
		
		onsearchpop : function (){
			this.router.navTo("notFound");
			
		},

		
		onNewItemPress : function(){//debugger
			// this.makeMap(127.11674377596071 , 37.484177185974836, null , false);  //여기서하면 main의 map을 바꾸겠지
			this.getRouter().navTo("Create");
		},
		
		//메인화면 열리자마자되는 바인딩
		_getSavedDataList : function(){  //debugger
			var that = this;
			var savedListItem = this.getView().byId("saveditemlist");
			// var placeName = this.getView().byId("keywordInput").getValue();
			// var cateCode = this.getView().byId("combocate").getSelectedKey();
			// if(!placeName || placeName.replace(/ /gi, "").length < 2){
			// 	this.getView().byId("keywordInput").setValue("");
			// 	MessageBox.error("검색어를 2글자 이상 입력해주세요.");
			// 	return;
			// }
			// if(cateCode === "All"){cateCode = "";}
			
			$.ajax({
				url : "/map_rimmy/api/v1/read",
				method : "GET",

				beforeSend: function () { 
					savedListItem.setBusy(true);
				},
				success : function(data,headers){ 
					if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
					if(data.length === 0) {//데이터가 없다면
						that.getView().byId("saveditemlist").setNoDataText("검색된 데이터가 없습니다.");
						that.makeMap(127.11674377596071 , 37.484177185974836, null, true); //시작점
					}else{//데이터가 있다면
						//데이터 바인딩
						if(!that.getView().getModel("savedMaplist")){
							var savedMapModel  = new JSONModel(data);
							that.getView().setModel(savedMapModel, "savedMaplist");
						}else{
							that.getView().getModel("savedMaplist").setData(data);
						}
						//데이터 바인딩 후 첫번째 리스트 값을 지도에 셋팅
						var mapXyData = that.getView().getModel("savedMaplist").getData()[0];
						//리스트 클릭
						that.getView().byId("saveditemlist").getAggregation("items")[0].setHighlight("Information");
						//지도에 셋팅
						that.makeMap(mapXyData.xyData[1] , mapXyData.xyData[0], mapXyData.placeName, true);
						
					}
					
					savedListItem.setBusy(false);
				},
				error : function(){
					savedListItem.setBusy(false);
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		}
		
		
		
		
	});
});